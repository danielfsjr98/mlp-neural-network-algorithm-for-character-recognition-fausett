package org.AICharacterRecognition.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class FileHandler {
    public ArrayList[] handleGetData(String pathname) {
        ArrayList<double[]> inputs = new ArrayList();

        ArrayList<double[] > outputs = new ArrayList();

        try {
            File file = new File(pathname);
            Scanner scanner = new Scanner(file);

            while (scanner.hasNext()) {
                String linha = scanner.nextLine();
                String[] data = linha.split(";");
                String[] row = data[0].split(",");

                double[] arrayInt = new double[row.length];
                for (int i = 0; i < row.length; i++) {
                    arrayInt[i] = Integer.valueOf(row[i].replace("\uFEFF", ""));
                }

                double[] input = Arrays.copyOfRange(
                        arrayInt,
                        0,
                        63
                );

                inputs.add(input);

                double[] output = Arrays.copyOfRange(
                        arrayInt,
                        63,
                        70
                );

                outputs.add(output);
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            System.out.println("Arquivo não encontrado: " + e.getMessage());
        }

        return new ArrayList[]{inputs, outputs};
    }
}
