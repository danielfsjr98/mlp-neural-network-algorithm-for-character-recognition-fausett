package org.AICharacterRecognition.utils;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.annotations.XYPointerAnnotation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseWheelEvent;

public class ChartGenerator {
    private XYSeries series;
    private XYSeriesCollection dataset;

    public ChartGenerator() {
        this.series = new XYSeries("Dados");
        this.dataset = new XYSeriesCollection();
    }

    public void plotErrorGraph() {
        // Adiciona a série de dados ao conjunto de dados
        this.dataset.addSeries(this.series);

        // Cria o gráfico de linha
        JFreeChart chart = ChartFactory.createXYLineChart(
                "Gráfico do erro médio por época",
                "Época",
                "Erro médio",
                this.dataset
        );

        // Cria o painel do gráfico
        ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setMouseWheelEnabled(true);
        chartPanel.addMouseWheelListener(new MouseAdapter() {
            @Override
            public void mouseWheelMoved(MouseWheelEvent e) {
                if (e.getWheelRotation() < 0) {
                    chartPanel.zoomInDomain(1.2, 1.2);
                    chartPanel.zoomInRange(1.2, 1.2);
                } else {
                    chartPanel.zoomOutDomain(1.2, 1.2);
                    chartPanel.zoomOutRange(1.2, 1.2);
                }
            }
        });

        // Encontra o mínimo global
        double minY = Double.MAX_VALUE;
        double minX = Double.MAX_VALUE;
        for (int i = 0; i < this.series.getItemCount(); i++) {
            double y = this.series.getY(i).doubleValue();
            if (y < minY) {
                minY = y;
                minX = this.series.getX(i).doubleValue();
            }
        }

        // Imprime os valores de minY e minX para depuração
        System.out.println("minY: " + minY);
        System.out.println("minX: " + minX);

        // Cria a anotação do mínimo global
        XYPointerAnnotation minAnnotation = new XYPointerAnnotation(
                String.format("Mínimo global: (%d, %.5f)", (int) minX, minY),
                minX, minY, Math.PI
        );
        minAnnotation.setPaint(Color.BLACK);
        minAnnotation.setArrowWidth(0.0);
        minAnnotation.setArrowLength(0.0);

        // Adiciona a anotação ao plot
        XYPlot plot = chart.getXYPlot();
        plot.addAnnotation(minAnnotation);


        // Ajusta a escala do gráfico
        plot.getRangeAxis().setRange(minY - 1, plot.getRangeAxis().getUpperBound());

        // Cria a janela do gráfico
        JFrame frame = new JFrame("Gráfico do erro médio por época");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add(chartPanel);
        frame.pack();
        frame.setVisible(true);
    }

    public void addDataToSeries(int x, double y) {
        this.series.add(x, y);
    }
}
