package org.AICharacterRecognition;

import org.AICharacterRecognition.MLP.MLP;
import org.AICharacterRecognition.utils.ChartGenerator;
import org.AICharacterRecognition.utils.FileHandler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        ChartGenerator chartGenerator = new ChartGenerator();

        MLP mlp = new MLP(
                63,
                24,
                7,
                0.001,
                chartGenerator
        );

        trainNeuralNetwork(
                mlp,
                "caracteres-limpo",
                "caracteres-ruido"
        );

        testNeuralNetwork(
                mlp,
                "caracteres_ruido20",
                0.8
        );
    }

    private static void trainNeuralNetwork(MLP mlp, String trainFileName, String validationFileName) {
        FileHandler fileHandler = new FileHandler();

        ArrayList[] trainData = fileHandler.handleGetData("src/caracteres-Fausett/"+trainFileName+".csv");
        ArrayList<double[]> trainInputs = trainData[0];
        ArrayList<double[]> trainOutputs = trainData[1];

        ArrayList[] validationData = fileHandler.handleGetData("src/caracteres-Fausett/"+validationFileName+".csv");
        ArrayList<double[]> validationInputs = validationData[0];
        ArrayList<double[]> validationOutputs = validationData[1];

        mlp.train(
                trainInputs,
                trainOutputs,
                validationInputs,
                validationOutputs,
                50000000,
                20000
        );
    }

    private static void testNeuralNetwork(MLP mlp, String testFileName, double threshold) {
        FileHandler fileHandler = new FileHandler();
        ArrayList[] testData = fileHandler.handleGetData("src/caracteres-Fausett/"+testFileName+".csv");

        ArrayList<double[]> testInputs = testData[0];

        String[] characters = new String[testInputs.size()];
        String[] cols = {"A", "B", "C", "D", "E", "J", "K"};
        int colCount = cols.length;

        for (int i = 0; i < characters.length; i++) {
            int row = i / colCount + 1;
            String col = cols[i % colCount];
            characters[i] = col + row;
        }

        for (int i = 0; i < characters.length; i++) {
            printCharacterByThresholdDecision(characters[i], mlp.predict(testInputs.get(i)), threshold);
        }
    }

    static void printCharacterByThresholdDecision(String expectedCharacter, double[] result, double threshold) {
        Map<Integer, String> dictionary = new HashMap<>();

        dictionary.put(0, "A");
        dictionary.put(1, "B");
        dictionary.put(2, "C");
        dictionary.put(3, "D");
        dictionary.put(4, "E");
        dictionary.put(5, "J");
        dictionary.put(6, "K");

        double max = Double.NEGATIVE_INFINITY;
        int index = 0;

        for (int i = 0; i < result.length; i++) {
            if (result[i] > max) {
                max = result[i];
                index = i;
            }
        }

        String charResult;
        if(max > threshold) {
            charResult = dictionary.get(index);
        } else {
            charResult = "Indefinido";
        }

        System.out.println(expectedCharacter + "=" + charResult);
    }
}