package org.AICharacterRecognition.MLP;

import org.AICharacterRecognition.utils.ChartGenerator;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class MLP {
    private final int inputSize; // quantidades de neuronios da camada de entrada
    private final int hiddenSize; // quantidades de neuronios da camada oculta
    private final int outputSize; // quantidades de neuronios de saida

    private double[][] inputHiddenWeights; // pesos das conexoes de camada de entrada -> camada oculta
    private double[][] hiddenOutputWeights; // matriz de pesos que define as conexões entre a camada oculta e a camada de saída da rede neural

    private double[] hiddenBiases; // bias de entrada p/ oculta
    private double[] outputBiases; // bias de camada oculta p/ saida

    private final double learningRate; // taxa de aprendizado do treinamento
    private final ChartGenerator chartGenerator;

    private final int columns;

    public MLP(int inputSize, int hiddenSize, int outputSize, double learningRate, ChartGenerator chartGenerator) {
        this.hiddenSize = hiddenSize;
        this.outputSize = outputSize;
        this.inputSize = inputSize;

        this.inputHiddenWeights = new double[inputSize][hiddenSize];
        this.hiddenOutputWeights = new double[hiddenSize][outputSize];

        this.hiddenBiases = new double[hiddenSize];
        this.outputBiases = new double[outputSize];

        this.learningRate = learningRate;

        this.columns = inputSize;

        this.chartGenerator = chartGenerator;

        this.initializeWeights();
    }

    private void initializeWeights() {
        // Inicializar os pesos das conexoes da camada de entrada p/ oculta aleatoriamente
        Random random = new Random();
        for (int i = 0; i < this.inputSize; i++) {
            for (int j = 0; j < this.hiddenSize; j++) {
                this.inputHiddenWeights[i][j] = random.nextDouble() - 0.5;
            }
        }

        // Inicializar os pesos das conexoes da camada oculta p/ saida aleatoriamente
        for (int i = 0; i < this.hiddenSize; i++) {
            for (int j = 0; j < outputSize; j++) {
                this.hiddenOutputWeights[i][j] = random.nextDouble() - 0.5;
            }
        }

        // Inicializar os pesos das bias de entrada p/ oculta aleatoriamente
        for (int i = 0; i < this.hiddenSize; i++) {
            this.hiddenBiases[i] = random.nextDouble() - 0.5;
        }

        // Inicializar os pesos das bias da oculta p/ saida aleatoriamente
        for (int i = 0; i < this.outputSize; i++) {
            this.outputBiases[i] = random.nextDouble() - 0.5;
        }
    }

    public void train(ArrayList<double[]> trainInputs,
                      ArrayList<double[]> trainOutputs,
                      ArrayList<double[]> validationInputs,
                      ArrayList<double[]> validationOutputs,
                      int epochs,
                      int patience
    ) {
        double bestError = Double.POSITIVE_INFINITY;
        int epochsWithoutImprovement = 0;
        double[][] bestInputHiddenWeights = new double[this.inputSize][this.hiddenSize];
        double[][] bestHiddenOutputWeights = new double[this.hiddenSize][this.outputSize];
        double[] bestInputHiddenBiases = new double[this.hiddenSize];
        double[] bestOutputBiases = new double[this.outputSize];

        DecimalFormat df = new DecimalFormat("#.##");
        for (int epoch = 0; (epoch < epochs && epochsWithoutImprovement < patience); epoch++) {
            double progress = (double) epoch / epochs * 100;
            System.out.println(df.format(progress) + "%");
            int currentIndex = epoch % trainInputs.size();

            double[] input = trainInputs.get(currentIndex);
            double[] output = trainOutputs.get(currentIndex);

            double[] hiddenOutput = new double[this.hiddenSize];
            double[] networkOutput = new double[this.outputSize];

            forwardPropagation(input, hiddenOutput, networkOutput);
            backwardPropagation(input, hiddenOutput, networkOutput, output);


            // validação
            double validationError = calculateValidationError(validationInputs, validationOutputs);
            System.out.println("bestError: " + bestError);

            this.chartGenerator.addDataToSeries(epoch, validationError);
            if (validationError < bestError) {
                System.out.println("MELHOROU");
                bestError = validationError;
                epochsWithoutImprovement = 0;

                for (int i = 0; i < this.inputSize; i++) {
                    bestInputHiddenWeights[i] = Arrays.copyOf(this.inputHiddenWeights[i], this.hiddenSize);
                }
                for (int i = 0; i < this.hiddenSize; i++) {
                    bestHiddenOutputWeights[i] = Arrays.copyOf(this.hiddenOutputWeights[i], this.outputSize);
                }
                bestInputHiddenBiases = Arrays.copyOf(this.hiddenBiases, this.hiddenSize);
                bestOutputBiases = Arrays.copyOf(this.outputBiases, this.outputSize);
            } else {
                epochsWithoutImprovement++;
            }

        }

        // quando for executar no container, comente a linha abaixo
        this.chartGenerator.plotErrorGraph();

        // Atualização dos pesos e vieses com os melhores valores obtidos
        this.inputHiddenWeights = bestInputHiddenWeights;
        this.hiddenOutputWeights = bestHiddenOutputWeights;
        this.hiddenBiases = bestInputHiddenBiases;
        this.outputBiases = bestOutputBiases;
    }

    private void forwardPropagation(double[] input, double[] hiddenOutput, double[] networkOutput) {
        for (int j = 0; j < this.hiddenSize; j++) {
            double weightedSum = this.hiddenBiases[j];
            for (int k = 0; k < this.columns; k++) {
                weightedSum += input[k] * this.inputHiddenWeights[k][j];
            }
            hiddenOutput[j] = tanh(weightedSum);
        }

        for (int j = 0; j < this.outputSize; j++) {
            double weightedSum = this.outputBiases[j];
            for (int k = 0; k < this.hiddenSize; k++) {
                weightedSum += hiddenOutput[k] * this.hiddenOutputWeights[k][j];
            }
            networkOutput[j] = tanh(weightedSum);
        }
    }

    private void backwardPropagation(double[] input, double[] hiddenOutput, double[] networkOutput, double[] output) {
        // calcula o erro na camada de saída
        double[] outputError = new double[this.outputSize];
        for (int j = 0; j < this.outputSize; j++) {
            outputError[j] = tanhDerivative(networkOutput[j]) * (output[j] - networkOutput[j]);
        }

        // atualiza os pesos entre as camadas oculta e de saída
        for (int j = 0; j < this.hiddenSize; j++) {
            for (int k = 0; k < this.outputSize; k++) {
                double delta = this.learningRate * outputError[k] * hiddenOutput[j];
                this.hiddenOutputWeights[j][k] += delta;
                this.outputBiases[k] += this.learningRate * outputError[k];
            }
        }

        // propaga o erro de volta para a camada oculta
        double[] hiddenError = new double[this.hiddenSize];
        for (int j = 0; j < this.hiddenSize; j++) {
            double weightedErrorSum = 0;
            for (int k = 0; k < this.outputSize; k++) {
                weightedErrorSum += outputError[k] * this.hiddenOutputWeights[j][k];
            }
            hiddenError[j] = tanhDerivative(hiddenOutput[j]) * weightedErrorSum;
        }

        // atualiza os pesos entre a camada de entrada e oculta
        for (int j = 0; j < this.hiddenSize; j++) {
            for (int k = 0; k < this.columns; k++) {
                this.inputHiddenWeights[k][j] += this.learningRate * hiddenError[j] * input[k];
            }
            this.hiddenBiases[j] += this.learningRate * hiddenError[j];
        }
    }

    private double calculateValidationError(ArrayList<double[]> validationInputs, ArrayList<double[]> validationOutputs) {
        double sum = 0;
        double error = 1;

        for (int n = 0; n < validationInputs.size(); n++) {
            double[] currValidationInput = validationInputs.get(n);
            double[] currValidationOutput = validationOutputs.get(n);

            double[] networkOutput = this.predict(currValidationInput);

            double[] outputError = new double[this.outputSize];
            for (int j = 0; j < this.outputSize; j++) {
                outputError[j] = currValidationOutput[j] - networkOutput[j];
            }

            error = mse(error, outputError);
            sum += error;
        }

        return sum / (validationInputs.size());
    }

    public double[] predict(double[] input) {
        double[] hiddenOutput = new double[this.hiddenSize];
        double[] networkOutput = new double[this.outputSize];

        forwardPropagation(input, hiddenOutput, networkOutput);

        return networkOutput;
    }

    private double mse(double error, double[] outputError) {
        // Cálculo do erro quadrático médio (MSE)
        double epochError = 0.0;
        for (int j = 0; j < this.outputSize; j++) {
            epochError += Math.pow(outputError[j], 2);
        }
        epochError /= this.outputSize;

        // Atualização do erro total
        error = (error + epochError) / 2;

        return error;
    }

    private double tanh(double x) {
        return Math.tanh(x);
    }

    private double tanhDerivative(double x) {
        double tanhX = tanh(x);
        return 1 - Math.pow(tanhX, 2);
    }
}