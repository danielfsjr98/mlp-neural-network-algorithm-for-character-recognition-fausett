FROM maven:3.8.3-openjdk-17 AS builder

COPY pom.xml /build/
WORKDIR /build/
RUN mvn -B dependency:go-offline

COPY . .
RUN mvn package

CMD java -jar /app/ReconhecimentoCaracteresFausettMLP-1.0-SNAPSHOT.jar

FROM openjdk:17-alpine

COPY --from=builder /build/target/ReconhecimentoCaracteresFausettMLP-1.0-SNAPSHOT-jar-with-dependencies.jar /app/ReconhecimentoCaracteresFausettMLP-1.0-SNAPSHOT-jar-with-dependencies.jar
COPY --from=builder /build/src/caracteres-Fausett /src/caracteres-Fausett

CMD java -jar /app/ReconhecimentoCaracteresFausettMLP-1.0-SNAPSHOT-jar-with-dependencies.jar